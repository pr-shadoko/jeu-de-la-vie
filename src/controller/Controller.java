package controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import model.Cell;
import model.Environment;
import model.EnvironmentRect;
import view.Board;
import view.BoardRect;
import view.MainWindow;
import view.PatternWindow;
import model.Patterns;
import model.Rule;
import model.RuleGreedy;

public final class Controller extends MouseAdapter implements ActionListener {

	private MainWindow mainWindow;
	private Board board;
	private Environment environment;
	private Point previousMousePosition;

	public Controller(MainWindow mainWindow) {
		this.setMainWindow(mainWindow);
	}

	@Override
	public void mouseMoved(MouseEvent me) {
		int newCellId = this.getBoard().visiblePosToSpot(me.getPoint());
		String pattern = (String) mainWindow.getCBPatterns().getSelectedItem();
		if (pattern.equals("Single cell")) {
			this.getBoard().setPreviousSpots(board.getHighlightedSpots());
			this.getBoard().setHighlightedSpots(new int[]{newCellId});
			if (board.getPreviousSpots() != null) {
				for (int i : board.getPreviousSpots()) {
					this.getBoard().repaintSpot(i);
				}
			}
			this.getBoard().repaintSpot(newCellId);
		} else {
			this.getBoard().setPreviousSpots(board.getHighlightedSpots());
			List<Integer> li = new LinkedList<Integer>();
			for (int x = 0; x < Patterns.CELLSWIDTH; x++) {
				for (int y = 0; y < Patterns.CELLSHEIGTH; y++) {
					if (Patterns.getHmPatterns().get(pattern + ".pattern")[x][y]) {
						int envW = ((EnvironmentRect) environment).getWidth();
						int posX = x + newCellId % envW; //(spot % this.nColumns)
						int posY = y + newCellId / envW;
						int id = posX + posY * envW;
						li.add(id);
					}
				}
			}
			int[] hSpots = new int[li.size()];
			int k = 0;
			for (Integer i : li) {
				hSpots[k] = i;
				k++;
			}
			this.getBoard().setHighlightedSpots(hSpots);

			if (board.getPreviousSpots() != null) {
				for (int i : board.getPreviousSpots()) {
					this.getBoard().repaintSpot(i);
				}
			}
			if (board.getHighlightedSpots() != null) {
				for (int i : board.getHighlightedSpots()) {
					this.getBoard().repaintSpot(i);
				}
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent me) {
		mouseAction(me.getPoint(), me.getModifiers());
	}

	@Override
	public void mousePressed(MouseEvent me) {
		mouseAction(me.getPoint(), me.getModifiers());
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		this.setPreviousMousePosition(null);
	}

	@Override
	public void mouseExited(MouseEvent me) {
		//int oldCellId = this.board.getHighlightedSpot();
		this.getBoard().setHighlightedSpots(null);
		for (int i : board.getPreviousSpots()) {
			this.getBoard().repaintSpot(i);
		}
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		mouseMoved(me);
		this.mouseAction(me.getPoint(), me.getModifiers());
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent me) {
		this.getBoard().zoom(-me.getWheelRotation());
		this.getMainWindow().getCentralArea().revalidate();
		this.getMainWindow().getCentralArea().repaint();
	}

	/**
	 * This method must be called by every listener method receiving an event
	 * with mouse click.mainWindow.getcbPatterns().setEnabled(true);
	 *
	 * @param mousePosition The mouse position.
	 * @param mouseModifiers The mouse buttons state.
	 */
	private void mouseAction(Point mousePosition, int mouseModifiers) {
		int cellId = this.getBoard().visiblePosToSpot(mousePosition);
		if (cellId == -1) {
			return;
		}


		if ((mouseModifiers & MouseEvent.BUTTON2_MASK) != 0) {
			if (this.getPreviousMousePosition() != null) {
				Point offset = new Point(
						(int) (mousePosition.getX() - this.getPreviousMousePosition().getX()),
						(int) (mousePosition.getY() - this.getPreviousMousePosition().getY()));
				this.getBoard().translateVisibleArea(offset);
			}
			this.setPreviousMousePosition(mousePosition);
		} else {
			Cell.State state = null;
			if ((mouseModifiers & MouseEvent.BUTTON1_MASK) != 0) {
				state = Cell.State.ALIVE;
			} else if ((mouseModifiers & MouseEvent.BUTTON3_MASK) != 0) {
				state = Cell.State.DEAD;
			}
			String pattern = (String) mainWindow.getCBPatterns().getSelectedItem();
			if (pattern.equals("Single cell")) {
				this.getEnvironment().setCellState(cellId, state);
			} else {
				for (int x = 0; x < Patterns.CELLSWIDTH; x++) {
					for (int y = 0; y < Patterns.CELLSHEIGTH; y++) {
						if (Patterns.getHmPatterns().get(pattern + ".pattern")[x][y]) {
							int envW = ((EnvironmentRect) environment).getWidth();
							int posX = x + cellId % envW;
							int posY = y + cellId / envW;
							int id = posX + posY * envW;
							this.getEnvironment().setCellState(id, state);
						}
					}
				}
			}
		}
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == mainWindow.getCbRule()) {
			if (mainWindow.getCbRule().getSelectedItem().equals("Basic rule")) {
				environment.setRule(new Rule());
			} else {
				environment.setRule(new RuleGreedy());
			}
		} else {
			JButton source = (JButton) ae.getSource();
			if (source == this.getMainWindow().getBNewGame()) {
				this.actionNewGame();
			} else if (source == this.getMainWindow().getBRandomize()) {
				this.actionRandomize();
			} else if (source == this.getMainWindow().getBClear()) {
				this.getEnvironment().clear();
			} else if (source == this.getMainWindow().getBPlay()) {
				this.actionPlay();
			} else if (source == this.getMainWindow().getBPause()) {
				this.actionPause();
			} else if (source == this.getMainWindow().getBNextStep()) {
				this.actionNextStep();
			} else if (source == this.getMainWindow().getBNewPattern()) {
				this.actionNewPattern();
			}
		}
	}

	private void actionNewGame() {
		int width = 200;
		int height = 150;
		if (mainWindow.getCbRule().getSelectedItem().equals("Basic rule")) {
			this.setEnvironment(new EnvironmentRect(width, height, new Rule()));
		} else {
			this.setEnvironment(new EnvironmentRect(width, height, new RuleGreedy()));
		}

		this.setBoard(new BoardRect(width, height, this.getEnvironment()));

		this.getEnvironment().addObserver(this.getBoard());
		this.getBoard().registerController(this);

		this.getMainWindow().startNewGame(this.getBoard());
		this.getMainWindow().getBRandomize().setEnabled(true);
		this.getMainWindow().getBPlay().setEnabled(true);
		this.getMainWindow().getBPause().setEnabled(false);
		this.getMainWindow().getBClear().setEnabled(true);
		this.getMainWindow().getBNextStep().setEnabled(true);
	}

	private void actionRandomize() {
		this.getEnvironment().randomize();
	}

	private void actionPlay() {
		this.getEnvironment().play();
		this.getMainWindow().getBPlay().setEnabled(false);
		this.getMainWindow().getBPause().setEnabled(true);
		this.getMainWindow().getBNextStep().setEnabled(false);
	}

	private void actionPause() {
		this.getEnvironment().pause();
		this.getMainWindow().getBPlay().setEnabled(true);
		this.getMainWindow().getBPause().setEnabled(false);
		this.getMainWindow().getBNextStep().setEnabled(true);
	}

	private void actionNextStep() {
		this.getEnvironment().computeNextStep();
	}

	private void actionNewPattern() {
		new PatternWindow(mainWindow);
		mainWindow.refreshPatternsList();
	}

	private void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}

	private void setBoard(Board board) {
		this.board = board;
	}

	private void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	private void setPreviousMousePosition(Point previousMousePosition) {
		this.previousMousePosition = previousMousePosition;
	}

	private MainWindow getMainWindow() {
		return this.mainWindow;
	}

	private Board getBoard() {
		return this.board;
	}

	private Environment getEnvironment() {
		return this.environment;
	}

	private Point getPreviousMousePosition() {
		return previousMousePosition;
	}
}
