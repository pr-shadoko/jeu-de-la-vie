/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jeudelavie;

import controller.Controller;
import view.MainWindow;
import view.PatternWindow;

/**
 *
 * @author tom
 */
public class Main {

	public static void main(String[] args) {
		MainWindow mw = new MainWindow();
		Controller c = new Controller(mw);
		mw.registerController(c);
	}
}
