package model;

import java.util.ArrayList;

public class Cell {

	public enum State {

		DEAD, ALIVE
	};
	private State currentState;
	private State nextState;
	private ArrayList<Cell> neighbors;
	private int id;

	public Cell(int id, State current) {
		this.id = id;
		currentState = current;
		nextState = current;
		neighbors = null;
	}

	public Cell(int id) {
		this(id, State.DEAD);
	}

	public void stepForward() {
		this.currentState = this.nextState;
	}

	public final void setCurrentState(State currentState) {
		this.currentState = currentState;
	}

	public final void setNextState(State nextState) {
		this.nextState = nextState;
	}

	public final void setNeighbors(ArrayList<Cell> neighbors) {
		this.neighbors = neighbors;
	}

	public final State getCurrentState() {
		return currentState;
	}

	public final State getNextState() {
		return nextState;
	}

	public final ArrayList<Cell> getNeighbors() {
		return neighbors;
	}

	public final int getId() {
		return this.id;
	}
}
