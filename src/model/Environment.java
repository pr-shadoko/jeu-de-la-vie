package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public abstract class Environment extends Observable {

	protected Cell[] cells;
	protected HashSet<Cell> aliveCells;
	protected Rule rule;
	protected int stepPeriod;
	protected Timer timer;

	public Environment(int cellCount, Rule rule) {
		super();
		this.setAliveCells(new HashSet<Cell>());
		this.setCells(new Cell[cellCount]);
		for (int i = 0; i < cellCount; ++i) {
			this.setCell(i, new Cell(i));
		}
		this.rule = rule;
		this.stepPeriod = 75;
	}

	/**
	 * Sets the state of given cell.
	 *
	 * @param cellId The identifier of the cell.
	 * @param state The new cell state.
	 */
	public synchronized void setCellState(int cellId, Cell.State state) {
		if (state == Cell.State.ALIVE) {
			this.getAliveCells().add(this.getCell(cellId));
		} else if (state == Cell.State.DEAD) {
			this.getAliveCells().remove(this.getCell(cellId));
		}
		this.getCell(cellId).setCurrentState(state);
		this.getCell(cellId).setNextState(state);
		this.setChanged();
		this.notifyObservers(new Integer(cellId));
	}

	/**
	 *
	 */
	public synchronized void computeNextStep() {

		HashSet<Cell> aliveCellsNeighbors = new HashSet<Cell>();
		HashSet<Cell> newAliveCells = new HashSet<Cell>();
		for (Cell c : this.getAliveCells()) {

			//Remove the cell from the living cell list if its dead
			if (this.rule.nextState(c) == Cell.State.ALIVE) {
				newAliveCells.add(c);
			}

			//Get the dead neighbors of living cells in a list to update them
			ArrayList<Cell> neighbors = c.getNeighbors();

			for (Cell n : neighbors) {
				if (n.getCurrentState() == Cell.State.DEAD) {
					aliveCellsNeighbors.add(n);
				}
			}

		}
		//Update the dead cells and put them in the living cells list if necessary
		for (Cell c : aliveCellsNeighbors) {
			this.rule.nextState(c);
			if (c.getNextState() == Cell.State.ALIVE) {
				newAliveCells.add(c);
			}
		}
		this.setAliveCells(newAliveCells);
		this.stepForward();
	}

	/**
	 * Sets the next step to be new current step.
	 */
	protected void stepForward() {
		for (Cell c : this.getCells()) {
			c.stepForward();
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Generates a random cell field.
	 */
	public void randomize() {
		Random rand = new Random();
		for (int i = 0; i < this.cells.length; ++i) {
			this.setCellState(i, (rand.nextBoolean()) ? (Cell.State.ALIVE) : (Cell.State.DEAD));
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Empties the cell field.
	 */
	public void clear() {
		for (int i = 0; i < this.cells.length; ++i) {
			this.setCellState(i, Cell.State.DEAD);
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Initializes cells neighbors.
	 */
	abstract protected void initNeighboring();

	public void play() {
		this.timer = new Timer();
		this.timer.schedule(new Player(), 0, stepPeriod);
	}

	public void pause() {
		this.timer.cancel();
	}

	protected class Player extends TimerTask {

		@Override
		public void run() {
			computeNextStep();
		}
	}

	private void setCells(Cell[] cells) {
		this.cells = cells;
	}

	private void setCell(int cellId, Cell cell) {
		this.cells[cellId] = cell;
	}

	private synchronized void setAliveCells(HashSet<Cell> aliveCells) {
		this.aliveCells = aliveCells;
	}

	private Cell[] getCells() {
		return this.cells;
	}

	private Cell getCell(int i) {
		return this.cells[i];
	}

	public HashSet<Cell> getAliveCells() {
		return this.aliveCells;
	}
        
       public void setRule(Rule rule){
            this.rule=rule;
        }
}
