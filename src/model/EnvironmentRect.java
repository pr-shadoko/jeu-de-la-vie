package model;

import java.util.ArrayList;

public class EnvironmentRect extends Environment {

	private final int width;
	private final int height;

	public EnvironmentRect(int dimension, Rule rule) {
		super(dimension * dimension, rule);
		width = dimension;
		height = dimension;

	}

	public EnvironmentRect(int width, int height, Rule rule) {
		super(width * height, rule);
		this.width = width;
		this.height = height;
		initNeighboring();
	}

	protected final void initNeighboring() {
		for (int i = 0; i < cells.length; ++i) {
			ArrayList<Cell> neighbors = new ArrayList<Cell>();
			ArrayList<Integer> indices = new ArrayList<Integer>();
			for (int x = -1; x <= 1; ++x) {
				for (int y = -1; y <= 1; ++y) {
					indices.add(i + x + y * getWidth());
				}
			}
			indices.remove(new Integer(i));
			if (spotX(i) == 0) {
				indices.remove(new Integer(i - getWidth() - 1));
				indices.remove(new Integer(i - 1));
				indices.remove(new Integer(i + getWidth() - 1));
			}
			if (spotY(i) == 0) {
				indices.remove(new Integer(i - getWidth() - 1));
				indices.remove(new Integer(i - getWidth()));
				indices.remove(new Integer(i - getWidth() + 1));
			}
			if (spotX(i) == getWidth() - 1) {
				indices.remove(new Integer(i - getWidth() + 1));
				indices.remove(new Integer(i + 1));
				indices.remove(new Integer(i + getWidth() + 1));
			}
			if (spotY(i) == getHeight() - 1) {
				indices.remove(new Integer(i + getWidth() - 1));
				indices.remove(new Integer(i + getWidth()));
				indices.remove(new Integer(i + getWidth() + 1));
			}
			for (Integer ind : indices) {
				neighbors.add(cells[ind]);
			}
			cells[i].setNeighbors(neighbors);
		}
	}

	public int spotX(int spotNumber) {
		return spotNumber % getWidth();
	}

	public int spotY(int spotNumber) {
		return spotNumber / getWidth();
	}

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }
}
