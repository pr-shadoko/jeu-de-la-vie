/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author leo
 */
public class Patterns {

	static private HashMap<String, boolean[][]> hmPatterns;
	public static final int CELLSWIDTH = 20;
	public static final int CELLSHEIGTH = 20;

	public static void save(String fname, boolean[][] cells) {
		(new File("patterns")).mkdirs();
		File f = new File("patterns/" + fname + ".pattern");
		try {
			FileWriter fw = new FileWriter(f);
			for (int x = 0; x < CELLSWIDTH; x++) {
				for (int y = 0; y < CELLSHEIGTH; y++) {
					if (cells[x][y]) {
						fw.write(x);
						fw.write(y);
					}
				}
			}
			fw.close();

		} catch (IOException e) {
			System.err.println(e.toString());
		}
	}

	public static String[] getPatternsList() {
		hmPatterns = new HashMap<String, boolean[][]>();
		File[] files = null;
		File directory = new File("patterns");
		files = directory.listFiles();
		List<String> fnames = new LinkedList<String>();
		fnames.add("Single cell");
		for (File f : files) {
			String fname = f.getName();
			int s = fname.length();
			if (f.getName().substring(s - 8).equals(".pattern")) {
				fnames.add(f.getName().substring(0, s - 8));
				try {
					boolean[][] cellsPos = new boolean[CELLSWIDTH][CELLSHEIGTH];
					FileReader fr = new FileReader(f);
					int posX = fr.read();
					int posY = fr.read();
					while (posX != -1) {
						cellsPos[posX][posY] = true;
						posX = fr.read();
						posY = fr.read();
					}
					fr.close();
					getHmPatterns().put(fname, cellsPos);
				} catch (IOException e) {
					System.err.println(e);
				}

			}
		}
		return fnames.toArray(new String[]{});
	}

	/**
	 * @return the hmPatterns
	 */
	public static HashMap<String, boolean[][]> getHmPatterns() {
		return hmPatterns;
	}
}
