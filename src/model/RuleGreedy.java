/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author leo
 */
public class RuleGreedy extends Rule{
    @Override
    public Cell.State nextState(Cell c) {
        	ArrayList<Cell> neighbors = c.getNeighbors();
		int aliveNeighborsCount = 0;
		Cell.State nextState = Cell.State.DEAD;
		for (Cell n : neighbors) {
			if (n.getCurrentState() == Cell.State.ALIVE) {
				aliveNeighborsCount++;
			}
		}
		//If the cell is alive
		if (c.getCurrentState() == Cell.State.ALIVE) {
			if (aliveNeighborsCount < 5) {
				nextState = Cell.State.ALIVE;
			}
		} else if (aliveNeighborsCount > 2) {
			nextState = Cell.State.ALIVE;
		}
		c.setNextState(nextState);
		return nextState;
    }
    
}
