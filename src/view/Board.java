package view;

import controller.Controller;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Observer;
import javax.swing.JPanel;

import model.Environment;

public abstract class Board extends JPanel implements Observer {

	protected Environment environment;
	protected int scale;
	protected int[] highlightedSpots;
	protected int[] previousSpots;
	protected Dimension availableAreaDimension;
	protected Point visibleAreaOffset;

	public Board(Environment environment) {
		super();
		this.setEnvironment(environment);
		this.setScale(1);
		this.setHighlightedSpots(null);
		this.setPreviousSpots(null);
		this.setVisibleAreaOffset(new Point());
	}

	public final void registerController(Controller controller) {
		this.addMouseListener(controller);
		this.addMouseMotionListener(controller);
		this.addMouseWheelListener(controller);
	}

	/**
	 * Get the identifier of the spot at a given position in the visible part of
	 * the board.
	 *
	 * @param pos Coordinates in the board.
	 * @return the identifier of the corresponding spot.
	 */
	abstract public int visiblePosToSpot(Point pos);

	/**
	 * Gets the position in the visible part of the board of a spot.
	 *
	 * @param spot The spot identifier.
	 * @return the coordinates of the upper-left corner of the spot.
	 */
	abstract public Point spotToVisiblePos(int spotId);

	/**
	 * Get the identifier of the spot at a given position in the board.
	 *
	 * @param pos Coordinates in the board.
	 * @return the identifier of the corresponding spot.
	 */
	abstract public int globalPosToSpot(Point pos);

	/**
	 * Gets the position of a spot.
	 *
	 * @param spot The spot identifier.
	 * @return the coordinates of the upper-left corner of the spot.
	 */
	abstract public Point spotToGlobalPos(int spotId);

	/**
	 * Repaints only the area corresponding to a given spot.
	 *
	 * @param spot The spot identifier.
	 */
	abstract public void repaintSpot(int spotId);

	public void translateVisibleArea(Point offset) {
		int offsetX = (int) (this.getVisibleAreaOffset().getX() - offset.getX());
		int offsetY = (int) (this.getVisibleAreaOffset().getY() - offset.getY());
		this.setVisibleAreaOffset(new Point(offsetX, offsetY));
		this.revalidate();
		this.repaint();
	}

	abstract protected boolean isSpotVisible(int spotId);

	public final void zoom(int zoomIncrement) {
		int previousScale = this.getScale();
		this.setScale(Math.max(this.getScale() + zoomIncrement, 1));

		Point offset = new Point(
				(int) (this.getVisibleAreaOffset().getX() * this.getScale() / previousScale),
				(int) (this.getVisibleAreaOffset().getY() * this.getScale() / previousScale));

		updateBoardDimension();
		this.setVisibleAreaOffset(offset);
	}

	public final void updateBoardDimension() {
		Dimension newDim = new Dimension(
				Math.min((int) this.getBaseDimension().getWidth(), (int) this.getAvailableAreaDimension().getWidth()),
				Math.min((int) this.getBaseDimension().getHeight(), (int) this.getAvailableAreaDimension().getHeight()));
		this.setPreferredSize(newDim);
	}

	protected final void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	protected final void setScale(int scale) {
		this.scale = scale;
	}

	public final void setHighlightedSpots(int[] highlightedSpots) {
		this.previousSpots = this.highlightedSpots;
		this.highlightedSpots = highlightedSpots;
	}

	public final void setPreviousSpots(int[] previousSpots) {
		this.previousSpots = previousSpots;
	}

	public final void setAvailableAreaDimension(Dimension availableAreaDimension) {
		Dimension previousDim = this.getAvailableAreaDimension();
		this.availableAreaDimension = availableAreaDimension;
		this.updateBoardDimension();
		Point diff;
		if (previousDim == null) {
			diff = new Point(
					(int) -(this.getBaseDimension().getWidth() - (Math.min((int) this.getBaseDimension().getWidth(), (int) this.getAvailableAreaDimension().getWidth()))) / 2,
					(int) -(this.getBaseDimension().getHeight() - (Math.min((int) this.getBaseDimension().getHeight(), (int) this.getAvailableAreaDimension().getHeight()))) / 2);
		} else {
			diff = new Point(
					(int) (this.getAvailableAreaDimension().getWidth() - previousDim.getWidth()) / 2,
					(int) (this.getAvailableAreaDimension().getHeight() - previousDim.getHeight()) / 2);
		}
		this.translateVisibleArea(diff);
	}

	private void setVisibleAreaOffset(Point visibleAreaOffset) {
		this.visibleAreaOffset = new Point(
				(int) Math.min(Math.max(visibleAreaOffset.getX(), 0), Math.max(this.getBaseDimension().getWidth() - this.getSize().getWidth(), 0)),
				(int) Math.min(Math.max(visibleAreaOffset.getY(), 0), Math.max(this.getBaseDimension().getHeight() - this.getSize().getHeight(), 0)));
	}

	abstract protected Dimension getBaseDimension();

	protected final Environment getEnvironment() {
		return this.environment;
	}

	protected final int getScale() {
		return this.scale;
	}

	public final int[] getHighlightedSpots() {
		return highlightedSpots;
	}

	public final int[] getPreviousSpots() {
		return previousSpots;
	}

	protected final Dimension getAvailableAreaDimension() {
		return this.availableAreaDimension;
	}

	protected final Point getVisibleAreaOffset() {
		return this.visibleAreaOffset;
	}
}
