package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Observable;
import model.Cell;
import model.Environment;

public class BoardRect extends Board {

	private int nColumns;
	private int nRows;

	public BoardRect(int nColumns, int nRows, Environment env) {
		super(env);
		this.setScale(4);
		this.nColumns = nColumns;
		this.nRows = nRows;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

		HashSet<Cell> cells = this.environment.getAliveCells();
		g.setColor(Color.white);

		for (Cell c : cells) {
			if (this.isSpotVisible(c.getId())) {
				Point pos = this.spotToVisiblePos(c.getId());
				g.fillRect((int) pos.getX(), (int) pos.getY(), this.getScale(), this.getScale());
			}
		}

		if (highlightedSpots != null) {
			for (int spot : highlightedSpots) {
				Point pos = this.spotToVisiblePos(spot);
				g.setColor(Color.red);
				g.drawRect((int) pos.getX(), (int) pos.getY(), this.getScale() - 1, this.getScale() - 1);
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg != null) {
			this.repaintSpot((Integer) arg);
			if (previousSpots != null) {
				for (int spot : previousSpots) {
					this.repaintSpot(spot);
				}
			}
		} else {
			this.repaint();
		}
	}

	@Override
	public void repaintSpot(int spot) {
		this.repaint(new Rectangle(spotToVisiblePos(spot), new Dimension(this.getScale(), this.getScale())));
	}

	@Override
	protected boolean isSpotVisible(int spotId) {
		Rectangle spot = new Rectangle(
				this.spotToGlobalPos(spotId),
				new Dimension(this.getScale(), this.getScale()));
		Rectangle visibleArea = new Rectangle(
				this.getVisibleAreaOffset(),
				this.getSize());
		return visibleArea.intersects(spot);
	}

	@Override
	public int visiblePosToSpot(Point pos) {
		Point p = new Point(pos);
		p.translate(
				(int) (this.getVisibleAreaOffset().getX()),
				(int) (this.getVisibleAreaOffset().getY()));
		return this.globalPosToSpot(p);
	}

	@Override
	public Point spotToVisiblePos(int spotId) {
		Point p = this.spotToGlobalPos(spotId);
		p.translate(
				(int) (-this.getVisibleAreaOffset().getX()),
				(int) (-this.getVisibleAreaOffset().getY()));
		return p;
	}

	@Override
	public int globalPosToSpot(Point pos) {
		double x = pos.getX() / this.getScale();
		double y = pos.getY() / this.getScale();
		if (x < 0 || y < 0 || x >= this.nColumns || y >= this.nRows) {
			return -1;
		}
		return (int) x + this.nColumns * (int) y;
	}

	@Override
	public Point spotToGlobalPos(int spotId) {
		return new Point(this.getScale() * (spotId % this.nColumns), this.getScale() * (spotId / this.nColumns));
	}

	@Override
	protected Dimension getBaseDimension() {
		return new Dimension(this.nColumns * this.getScale(), this.nRows * this.getScale());
	}
}
