package view;

import model.Patterns;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import controller.Controller;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class MainWindow extends JFrame {

	private JPanel centralArea;
	private JPanel dock;
	private JButton bNewGame;
	private JButton bNewPattern;
	private JButton bRandomize;
	private JButton bClear;
	private JButton bPlay;
	private JButton bPause;
	private JButton bNextStep;
	private Board board;
	private JComboBox cbPatterns;
	private JComboBox cbRule;
	private JTextField tfHeight;
	private JTextField tfWidth;

	public MainWindow() {
		super();
		initGUI();

		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent ce) {
				if (getBoard() != null) {
					getBoard().setAvailableAreaDimension(getCentralArea().getSize());
					getCentralArea().revalidate();
					getCentralArea().repaint();
				}
			}
		});

		this.setTitle("A cell's life");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(800, 600));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initGUI() {
		this.setDock(new JPanel(new GridLayout(11, 1)));
		this.setCentralArea(new JPanel(new GridBagLayout()));
		this.getContentPane().add(this.getCentralArea(), BorderLayout.CENTER);
		this.getContentPane().add(this.getDock(), BorderLayout.EAST);
		this.setBNewGame(new JButton("New game"));
		this.setBRandomize(new JButton("Randomize"));
		this.setBClear(new JButton("Clear"));
		this.setBPlay(new JButton("Play"));
		this.setBPause(new JButton("Pause"));
		this.setBNextStep(new JButton("Next Step"));
		this.setBNewPattern(new JButton("New Pattern"));
		this.setCBPatterns(new JComboBox(Patterns.getPatternsList()));
		this.setCbRule(new JComboBox(new String[]{"Basic rule", "Greedy rule"}));
		this.setTfHeight(tfHeight);
		this.getDock().add(this.getBNewGame());
		this.getDock().add(this.getBRandomize());
		this.getDock().add(this.getBClear());
		this.getDock().add(this.getBPlay());
		this.getDock().add(this.getBPause());
		this.getDock().add(this.getBNextStep());
		this.getDock().add(new JLabel("Pattern :"));
		this.getDock().add(this.getCBPatterns());
		this.getDock().add(this.getBNewPattern());
		this.getDock().add(new JLabel("Rule :"));
		this.getDock().add(this.getCbRule());
		//this.getDock().add(new JLabel("Grid weight :"));
		//this.getDock().add(new JLabel("Grid height :"));
		this.getBRandomize().setEnabled(false);
		this.getBClear().setEnabled(false);
		this.getBPlay().setEnabled(false);
		this.getBPause().setEnabled(false);
		this.getBNextStep().setEnabled(false);
	}

	public void registerController(Controller controller) {
		this.getBNewGame().addActionListener(controller);
		this.getBRandomize().addActionListener(controller);
		this.getBClear().addActionListener(controller);
		this.getBPlay().addActionListener(controller);
		this.getBPause().addActionListener(controller);
		this.getBNextStep().addActionListener(controller);
		this.getBNewPattern().addActionListener(controller);
		this.getCbRule().addActionListener(controller);
	}

	public void startNewGame(Board board) {
		if (this.getBoard() != null) {
			this.getCentralArea().remove(this.getBoard());
		}
		this.getCentralArea().add(board);
		this.setBoard(board);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}

	public void refreshPatternsList() {
		String[] pats = Patterns.getPatternsList();
		this.getCBPatterns().removeAllItems();
		for (String s : pats) {
			this.getCBPatterns().addItem(s);
		}
		this.getCBPatterns().revalidate();
		this.getCBPatterns().repaint();
	}

	public final void setBoard(Board board) {
		this.board = board;
		this.board.setAvailableAreaDimension(this.getCentralArea().getSize());
	}

	private void setCentralArea(JPanel centralArea) {
		this.centralArea = centralArea;
	}

	private void setDock(JPanel dock) {
		this.dock = dock;
	}

	private void setBNewGame(JButton bNewGame) {
		this.bNewGame = bNewGame;
	}

	private void setBRandomize(JButton bRandomize) {
		this.bRandomize = bRandomize;
	}

	private void setBClear(JButton bClear) {
		this.bClear = bClear;
	}

	private void setBPlay(JButton bPlay) {
		this.bPlay = bPlay;
	}

	private void setBPause(JButton bPause) {
		this.bPause = bPause;
	}

	private void setBNextStep(JButton bNextStep) {
		this.bNextStep = bNextStep;
	}

	private void setBNewPattern(JButton bNewPattern) {
		this.bNewPattern = bNewPattern;
	}

	private void setCBPatterns(JComboBox cbPatterns) {
		this.cbPatterns = cbPatterns;
	}

	public void setCbRule(JComboBox cbRule) {
		this.cbRule = cbRule;
	}

	public void setTfHeight(JTextField tbHeight) {
		this.tfHeight = tbHeight;
	}

	public void setTfWidth(JTextField tbWeight) {
		this.tfWidth = tbWeight;
	}

	public final Board getBoard() {
		return this.board;
	}

	public final JPanel getCentralArea() {
		return this.centralArea;
	}

	private JPanel getDock() {
		return this.dock;
	}

	public final JButton getBNewGame() {
		return this.bNewGame;
	}

	public final JButton getBRandomize() {
		return this.bRandomize;
	}

	public final JButton getBClear() {
		return this.bClear;
	}

	public final JButton getBPlay() {
		return this.bPlay;
	}

	public final JButton getBPause() {
		return this.bPause;
	}

	public final JButton getBNextStep() {
		return this.bNextStep;
	}

	public final JButton getBNewPattern() {
		return this.bNewPattern;
	}

	public final JComboBox getCBPatterns() {
		return this.cbPatterns;
	}

	public JTextField getTfHeight() {
		return tfHeight;
	}

	public JTextField getTfWidth() {
		return tfWidth;
	}

	public JComboBox getCbRule() {
		return cbRule;
	}
}
